package com.bank.wawa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bank.wawa.dto.TransactionDetailsDto;
import com.bank.wawa.service.FundTranserService;

@RestController
@RequestMapping("/fundtrans")
public class FundTranserController {
	
	
	@Autowired
	private FundTranserService fundService;
	
	@PostMapping("/transfer")
	public ResponseEntity<String> fundTransfer(@RequestParam Long sourceAccount, @RequestParam  Long destAccount, @RequestParam double amt ) {	
		  String strRes=fundService.fundTransfer(sourceAccount, destAccount, amt);	
		 return new ResponseEntity<>(strRes,HttpStatus.OK);
	}
	
	@GetMapping("/report")
	public  ResponseEntity<List<TransactionDetailsDto>> getMontlyReport(@RequestParam Long accNumber,@RequestParam Integer month,@RequestParam Integer year){
		
		List<TransactionDetailsDto> listTransDetailsDto=fundService.getMontlyReport(accNumber, month, year);
		return new ResponseEntity<>(listTransDetailsDto,HttpStatus.OK);
	}
	
	@PostMapping("/mstransfer")
	public ResponseEntity<String> fundTransferByMobileNo(@RequestParam Long sourceMobileNo, @RequestParam  Long destMobileNo, @RequestParam double amount ) {	
		String strRes=null;
		try {
			  strRes=fundService.fundTransferByMobileNo(sourceMobileNo, destMobileNo, amount );
		 }catch(Exception e) {
			 strRes=e.getMessage();
		 }
			
		 return new ResponseEntity<>(strRes,HttpStatus.OK);
	}

}
