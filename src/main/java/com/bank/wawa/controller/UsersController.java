package com.bank.wawa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.wawa.dto.UserDto;
import com.bank.wawa.service.RegisterUserService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	private RegisterUserService userService;
		
	
	@PostMapping("/create")
	public ResponseEntity<Long> createUser(@RequestBody UserDto userDto) {
	    Long accNo= userService.createUser(userDto);
	    return new ResponseEntity<>(accNo,HttpStatus.CREATED); 
	}
	
	@GetMapping("/checkregister")
	public ResponseEntity<Boolean> checkMobileNumber(@RequestParam Long mobilenumber) {
		Boolean isValid=userService.checkMobileNumber(mobilenumber);
	  return new ResponseEntity<>(isValid,HttpStatus.OK);
		
	}
	
	@GetMapping("/status")
	public ResponseEntity<String> checkStatus() {
		return ResponseEntity.ok("Services is running...");
	}

}
