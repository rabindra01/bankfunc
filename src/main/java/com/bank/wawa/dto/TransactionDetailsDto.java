package com.bank.wawa.dto;

import java.util.Date;

public class TransactionDetailsDto {

	private Long transactionId;
	private Long	sourceAccount;	
	private Long	destAccount;	
	private double	amount;	
	private Date	requestedDate;
	
	
	public TransactionDetailsDto() {
		super();
	}
	
	public TransactionDetailsDto(Long transactionId, Long sourceAccount, Long destAccount, double amount,
			Date requestedDate) {
		super();
		this.transactionId = transactionId;
		this.sourceAccount = sourceAccount;
		this.destAccount = destAccount;
		this.amount = amount;
		this.requestedDate = requestedDate;
	}
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	public Long getSourceAccount() {
		return sourceAccount;
	}
	public void setSourceAccount(Long sourceAccount) {
		this.sourceAccount = sourceAccount;
	}
	public Long getDestAccount() {
		return destAccount;
	}
	public void setDestAccount(Long destAccount) {
		this.destAccount = destAccount;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

}
