package com.bank.wawa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ACCOUNT_INFO")
public class AccountInfo  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "account_number")
	private Long	accountNumber;	
	
	@Column(name = "user_id")
	private String	userid;	
	
	@Column(name = "account_Type")
	private	String acctountType;
	
	private double  balance;
	
	
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAcctountType() {
		return acctountType;
	}

	public void setAcctountType(String acctountType) {
		this.acctountType = acctountType;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}


}
