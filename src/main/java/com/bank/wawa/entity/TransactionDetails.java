package com.bank.wawa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "TRANACTION_DETAILS")
public class TransactionDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "transaction_Id")
	private Long transactionId;
	
	@Column(name = "source_account")
	private Long	sourceAccount;	
	@Column(name = "dest_account")
	private Long	destAccount;	
	private double	amount;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "requested_date")
	private Date	requestedDate;
	

	
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

    
	public Long getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(Long sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public Long getDestAccount() {
		return destAccount;
	}

	public void setDestAccount(Long destAccount) {
		this.destAccount = destAccount;
	}

	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}


}
