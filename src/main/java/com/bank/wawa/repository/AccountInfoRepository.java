package com.bank.wawa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.wawa.entity.AccountInfo;

@Repository
public interface AccountInfoRepository extends JpaRepository<AccountInfo, Long>{

	AccountInfo  findByUserid(String userId);

}
