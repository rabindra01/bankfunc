package com.bank.wawa.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.wawa.entity.TransactionDetails;

public interface FundTranserRepository extends JpaRepository<TransactionDetails, Long> {

	
	List<TransactionDetails> findByDestAccountAndRequestedDateBetween(Long accNumber, Timestamp fmTimestamp,
			Timestamp toTimestamp);
	
	

	

}
