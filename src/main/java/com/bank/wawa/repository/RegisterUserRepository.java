package com.bank.wawa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.wawa.entity.UserInfo;

@Repository
public interface RegisterUserRepository extends JpaRepository<UserInfo, String> {

	UserInfo findByMobileNo(Long mobilenumber);

}
