package com.bank.wawa.service;

import java.util.List;

import com.bank.wawa.dto.TransactionDetailsDto;

public interface FundTranserService {
	
	 public String fundTransfer(Long sourceAccount, Long destAccount, double amt );
	 
	 public  List<TransactionDetailsDto> getMontlyReport(Long accNumber,Integer month, Integer year);

	public String fundTransferByMobileNo(Long sourceMobileNo, Long destMobileNo, double amount) throws Exception;

}
