package com.bank.wawa.service;

import com.bank.wawa.dto.UserDto;


public interface RegisterUserService {
	
	public Long createUser( UserDto userDto);

	public Boolean checkMobileNumber(Long mobilenumber);

}
