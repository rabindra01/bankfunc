package com.bank.wawa.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.wawa.dto.TransactionDetailsDto;
import com.bank.wawa.entity.AccountInfo;
import com.bank.wawa.entity.TransactionDetails;
import com.bank.wawa.entity.UserInfo;
import com.bank.wawa.repository.AccountInfoRepository;
import com.bank.wawa.repository.FundTranserRepository;
import com.bank.wawa.repository.RegisterUserRepository;
import com.bank.wawa.service.FundTranserService;

@Service
public class FundTranserServiceImpl implements FundTranserService {
	
	@Autowired
	private FundTranserRepository fundtransRepository;
	
	@Autowired
	private AccountInfoRepository accRepository;
	
	@Autowired
	private RegisterUserRepository  userRepository;
    
	@Override
	@Transactional
	public String fundTransfer(Long sourceAccount, Long destAccount, double amt) {
		String retValue = "Fund is not transfer";
		AccountInfo destAccInf;
		AccountInfo sourceAccInf;
		try {
			Optional<AccountInfo> sourceAccInfo= accRepository.findById(sourceAccount);
		    Optional<AccountInfo> destAccInfo= accRepository.findById(destAccount);
		    if (destAccInfo.isPresent() && sourceAccInfo.isPresent()) {
		    	 sourceAccInf=sourceAccInfo.get();
		    	if(sourceAccInf.getBalance() < amt)
					retValue= "Amount is not sufficient";
		    	destAccInf=destAccInfo.get();
		    	sourceAccInf.setBalance(sourceAccInf.getBalance()-amt);
				destAccInf.setBalance(destAccInf.getBalance()+amt);
				accRepository.save(sourceAccInf);
				accRepository.save(destAccInf);
				
				TransactionDetails  transactionDetails= new TransactionDetails();
				transactionDetails.setSourceAccount(sourceAccount); 
				transactionDetails.setDestAccount(destAccount);
				transactionDetails.setAmount(amt);			
				transactionDetails.setRequestedDate(Timestamp.valueOf(LocalDateTime.now()));
				fundtransRepository.save(transactionDetails);
				retValue ="Fund is transfered";
		    }
		}catch(Exception e) {
			e.printStackTrace();
		}
		return retValue;		
	}

	@Override
	public List<TransactionDetailsDto> getMontlyReport(Long accNumber, Integer month, Integer year) {
		
		LocalDateTime framdate = LocalDateTime.of(year,month,1,00, 00);
        
	       YearMonth yearmonthObje=YearMonth.of(year,month);
	       LocalDateTime todate = LocalDateTime.of(year,month,yearmonthObje.lengthOfMonth(),23,59);
	       
	       Timestamp fmTimestamp=Timestamp.valueOf(framdate);
	       Timestamp toTimestamp=Timestamp.valueOf(todate);
		
	       List<TransactionDetails> transactionDetails=  fundtransRepository.findByDestAccountAndRequestedDateBetween(accNumber, fmTimestamp, toTimestamp);
	       List<TransactionDetailsDto> transactionDetailsDto=transactionDetails.stream().map(ta->new TransactionDetailsDto(ta.getTransactionId(),
	    		   ta.getSourceAccount(),ta.getDestAccount(),ta.getAmount(),ta.getRequestedDate())).collect(Collectors.toList());
	    return transactionDetailsDto;
	}

	@Override
	public String fundTransferByMobileNo(Long sourceMobileNo, Long destMobileNo, double amount) throws Exception {
		String success = null;
		UserInfo sourceUserInfo= userRepository.findByMobileNo(sourceMobileNo);
	    UserInfo destUserInfo= userRepository.findByMobileNo(destMobileNo);
	    if (sourceUserInfo == null) {
	    	throw new Exception("Source_User's mobileNo not registered");
	    }
	    if (destUserInfo == null) {
	    	throw new Exception("Destination_User's mobileNo not registered");
	    }	
	   
	         AccountInfo sourceAccInfo= accRepository.findByUserid(sourceUserInfo.getUserId());
             AccountInfo destAccInfo= accRepository.findByUserid(destUserInfo.getUserId());
             success= fundTransfer(sourceAccInfo.getAccountNumber(),destAccInfo.getAccountNumber(), amount);
	    
		     
		return success;
	}

	
    
}
