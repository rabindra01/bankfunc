package com.bank.wawa.service.impl;

import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.wawa.dto.UserDto;
import com.bank.wawa.entity.AccountInfo;
import com.bank.wawa.entity.UserInfo;
import com.bank.wawa.repository.AccountInfoRepository;
import com.bank.wawa.repository.RegisterUserRepository;
import com.bank.wawa.service.RegisterUserService;

@Service
public class RegisterUserServiceImpl implements RegisterUserService {
	
	@Autowired
	private RegisterUserRepository  userRepository;
	
	@Autowired
	private AccountInfoRepository accRepository;
	
	private double openingBal=10000.00;
	
	@Override
	public Long createUser(UserDto userDto ) {
		
		Long accNo= getUUIDGenerator();
		AccountInfo accInfo=new AccountInfo();
		accInfo.setAccountNumber(accNo);
		accInfo.setUserid(userDto.getUserId());
		accInfo.setAcctountType(userDto.getAcctountType());
		accInfo.setBalance(openingBal);
		
		UserInfo userInfo=new UserInfo();
		BeanUtils.copyProperties(userDto, userInfo);
		
		userRepository.save(userInfo);
		accRepository.save(accInfo);	
		return accNo;
	}
	
	private Long getUUIDGenerator() {
		UUID randomUUID = UUID.randomUUID();
		long uuNum=randomUUID.getLeastSignificantBits()+randomUUID.getMostSignificantBits();
		if(uuNum < 0){
			uuNum=uuNum * -1;
		}
		return  uuNum % 1000_0000_0000_0000L;
	}

	@Override
	public Boolean  checkMobileNumber(Long mobilenumber) {
		Boolean isValid=true;
		UserInfo userInfo=userRepository.findByMobileNo(mobilenumber);
		  if (userInfo == null) {
			  isValid= false;
		    }
		return isValid;
	
	}

}
